const express = require('express');
const mongoose =require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes.js');

const port = 5000;
const app = express();

require('dotenv').config();	

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

//routes
app.use('/api/users', userRoutes);


mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-remolona.mkzggg7.mongodb.net/b303-booking-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true	
});

mongoose.connection.on('error', () => console.log(`Cant Connect to DB`));
mongoose.connection.once('open', () => console.log(`Connected to DB`));









app.listen(process.env.PORT || port, () => {
	console.log(`running at localhost:${process.env.PORT || port}`);
})

module.exports = app;