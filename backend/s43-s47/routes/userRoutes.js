const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js')

router.post('/check-email', (request, response) => {

	UserController.checkEmailExists(request.body).the((result) => {
		response.send(result);
	})

})

module.exports = router;