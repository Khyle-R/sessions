const User = require('../models/User.js')

module.exports.checkEmailExists = (request_body) => {
	return User.find({email: request_body.email}).then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		if (result.length <= 0){
			return false;
		}
		return true;
	})
}