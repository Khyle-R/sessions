/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

	function getUserInfo() {
		let userinfo = {
			name: "Khyle",
			age: 22,
			address: "Caloocan city",
			isMarried: false,
			petName: "Chico"
		}
		console.log(userinfo);
	}

	getUserInfo();


/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/
	function getArtistsArray() {
		let artists = ["Paramore", "Lola Amour", "Franco", "Joji", "Ben & Ben"];

		console.log(artists);	
	}

	getArtistsArray();



/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getSongsArray() {
		let songs = ["Ain't it fun", "Hard times", "Pool", "Fake happy", "this is why", "False pretense"]

		console.log(songs);	
	}

	getSongsArray();


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getMoviesArray() {
		let movies = ["Tag", "Klaus", "Interstellar", "Meet  the Robinsons", "Luca"];

		console.log(movies);
	}

	getMoviesArray();

/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/
	// function getPrimeNumberArray() {
	// 	let prime = [2, 3, 5, 7, 11, 13, 17, 19]

	// 	console.log(prime);
	// }

	// getPrimeNumberArray();

	function getPrimeNumberArray1() {
		let prime = [2, 3, 5, 7, 11, 13, 17, 19];

		return prime;
	}



//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}