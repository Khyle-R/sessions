let http = require('http');

const port = 4000;

const app = http.createServer((req, res) => {

	if(req.url == '/greeting'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Hello Worldszx');

	} else if (req.url == '/homepage'){
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Hello homepage');
	}

});

app.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);