
const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js');


router.post('/', (request, response) => {
	TaskController.createTask(request.body).then(result => {
		response.send(result);
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result =>{
		response.send(result);
	})
	
})

// router.put('/', (request, response) => {
// 	TaskController.PlaceholderFuncrtion().then(result =>{
// 		response.send(result);
// 	})
	
// })

module.exports = router;