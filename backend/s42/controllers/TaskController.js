const Task = require('../models/Task.js')

module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if(error){
			return {
				message: error.message 
			}
		}

		return {
			tasks: result 
		}
	})
}

module.exports.createTask = (request_body) => {
	return Task.findOne({name: request_body.name}).then((result, error) => {
		// Check if the task already exists by utilizing the 'name' property. If it does, then return a response to the user.
		if(result != null && result.name == request_body.name){

			return {
				message: "Duplicate user Found!"
			}

		} else {
			// 1. Create a new instance of the task model which will contain the properties required based on the schema
			let newTask = new Task({
				name: request_body.name 
			});

			// 2. Save the new task to the database
			newTask.save().then((savedTask, error) => {
				if(error){
					return {
						message: error.message 
					};
				}

				return {
					message: error.message	
				};
			})
		}
	})
}