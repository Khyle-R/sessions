const express = require('express');

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.listen(port, () => console.log(`Server is running at port ${port}`));


app.get('/', (request, response) => {
	response.send('Hello world');
})

app.delete('/delete-user', (request, response) => {
	//response.send('Hello world');
})

process.on('SIGINT', function() {
  console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
  // some other closing procedures go here
  process.exit(0);
});

module.exports = app;