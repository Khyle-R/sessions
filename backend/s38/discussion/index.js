let http = require("http");

const port = 4000;

//db
let users = [
	{
		"name": "paopao",
		"email": "paopao@hotmail.com"
	},
	{
		"name": "sishomaru",
		"email": "sishomaru@gmail.com"
	}
]

const app = http.createServer((req, res) => {

	//GET
    if(req.url  == '/items' && req.method == 'GET') {

        res.writeHead(200, {'Content-Type': 'text/plain'});

        res.end('Data retrieved from the database')
    }

    //POST
    if(req.url == '/addcourse' && req.method == 'POST') {
    	res.writeHead(200, {'Content-Type': 'text/plain'});
    	res.end('Add a course to our resources');
    }

    // POST
    if(req.url == '/items' && req.method == 'POST') {
    	res.writeHead(200, {'Content-Type': 'text/plain'});
    	res.end('Data sent to the database!');
    }

    // getting items from database
    if(req.url== '/users' && req.method == 'GET'){
    	res.writeHead(200, {'Content-Type': 'application/json'});

    	res.end(JSON.stringify(users));	
    }
})

app.listen(port, () => console.log(`Server is running at localhost: ${port}`));



// POST
    // if(req.url == '/addcourse' && req.method == 'POST') {
    // 	res.writeHead(200, {'Content-Type': 'text/plain'});
    // 	res.end('Add a course to our resources');
    // }