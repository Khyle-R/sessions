async function createToDo() {
	const apiUrl = 'https://jsonplaceholder.typicode.com/todos';


	const data = {
    	title: "Oui Oui",
    	completed: true,
    	userId: 1,
  	};

 
	const response = await fetch(apiUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });


    const createdlist = await response.json();
    return createdlist;
  
}

createToDo();

console.log(createToDo());