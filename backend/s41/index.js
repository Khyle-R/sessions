const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config()

const app = express();
const port = 4000;

//MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@batch303-remolona.mkzggg7.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true	
});


let database = mongoose.connection;
database.on('error', () => console.log(`Connection error`));
database.once('open', () => console.log(`Connected to MongoDB`));

//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));


const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {
		
		if(result != null && result.name == request.body.name){

			return response.send("Duplicate task found");

		} else {

			let newTask = new Task({
				name: request.body.name
			});

			newTask.save().then((savedTask,error) => {
				if(error){

					return response.send({
						message: error.message
					});	
				}

				return response.send(201, "New Task Created");
			});
		}
	});
});

app.listen(port, () => console.log(`Server is running at port ${port}`));

module.exports = app;