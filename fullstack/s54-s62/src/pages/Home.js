import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from '../components/Highlights.js';

export default function Home(){
	const title = "Khyle's Page";
	const subtitle = "A path to greatness";
	const buttonText = "Welcome";
	const redirectPath = "home";

	return(
		<>
			<Banner title={title} subtitle={subtitle} buttonText={buttonText} redirectPath={redirectPath} />
			<FeaturedCourses />
			<Highlights/>
		</>
	)
}