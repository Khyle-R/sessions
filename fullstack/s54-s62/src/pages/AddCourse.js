import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function AddCourse(){
	const { user } = useContext(UserContext);

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	function Add(event){
		// Prevents page load upon form submission
		event.preventDefault();

		// Sends a request to the /register endpoint which will include all the fields necessary for that route.
		fetch('http://localhost:4000/courses/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
			})
		}).then(response => response.json()).then(result => {
			if(result){
				// Resets all input fields
				setName("")
				setDescription("")
				setPrice("")

				Swal.fire({
                    title: "Course Added",
                    icon: "success",
                    text: "zxcmzxczxczxczxc"
                })

			} else {
				Swal.fire({
                    title: "Something Went Wrong",
                    icon: "success",
                    text: "please try again"
                })
			}
		})
	}

	// The useEffect arrow function will trigger everytime there are changes in the data within the 2nd argument array.
	// Note: If the 2nd argument array is empty, then the function will only run upon the initial loading of the component.
	useEffect(() => {
		// Checks if all fields aren't empty, if password and confirm password fields are matching, and mobile number is 11 characters.
		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, price]);

	console.log(user.isAdmin);

	return(
		
		(user.isAdmin !== true) ?


        	<Navigate to="/" />
        	:

			<Form onSubmit={(event) => Add(event)}>
		        <h1 className="my-5 text-center">Add Course</h1>
		            <Form.Group>
		                <Form.Label>First Name:</Form.Label>
		                <Form.Control 
		                	type="text" 
		                	placeholder="Enter Name" 
		                	required
		                	value={name}
		                	onChange={event => {setName(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Desciption:</Form.Label>
		                <Form.Control 
		                	type="text" 
		                	placeholder="Enter Desciption" 
		                	required
		                	value={description}
		                	onChange={event => {setDescription(event.target.value)}}
		                />
		            </Form.Group>
		            <Form.Group>
		                <Form.Label>Price: </Form.Label>
		                <Form.Control 
		                	type="number" 
		                	placeholder="Enter Price" 
		                	required
		                	value={price}
		                	onChange={event => {setPrice(event.target.value)}}
		                />
		            </Form.Group>

		        <Button variant="primary" type="submit" disabled={isActive === false}>Submit</Button>          
		    </Form>
			
        
			
	)
}