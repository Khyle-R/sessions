import Banner from '../components/Banner.js';

export default function NotFound(){
	const title = "ERROR 404";
	const subtitle = "The page you are looking for is not found";
	const buttonText = "Go back";
	const redirectPath = "notfound";

	return(
		<>
			<Banner title={title} subtitle={subtitle} buttonText={buttonText} redirectPath={redirectPath} />
		</>
	)
}