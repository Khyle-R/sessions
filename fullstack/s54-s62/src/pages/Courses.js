import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';

export default function Courses() {
	const { user } = useContext(UserContext);
	const [courses, setCourses] = useState([]);

	// useEffect(() => {
    // 	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
    //   	.then(res => res.json())
    // 	.then(data => {
    //     	console.log(data);
    //     	setCourses(data);
    //   	});
  	// }, []);

  	const fetchData = () => {
    	fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
      	.then(res => res.json())
    	.then(data => {
        	console.log(data);
        	setCourses(data);
      	});
  	};

  	useEffect(() => {
  		fetchData();
  	}, []);


	return (
    	<>
    		<h1 className="text-center">Courses</h1>
      		{
      			user.isAdmin ? 
      				(
        				<AdminView coursesData={courses} fetchData={fetchData}/>
      				) 
      					: 
      				(
        				<UserView coursesData={courses} />
      				)
      		}
   		</>
  );
}
