import React, { useState, useContext } from 'react';
import { Button, Form } from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function UpdateProfile() {
  const { user } = useContext(UserContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');

  const handleUpdate = () => {
   
    fetch(`http://localhost:4000/users/profile`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
      	firstName: firstName,
      	lastName: lastName,
      	mobileNo: mobileNo
      })
    })
    .then(response => response.json())
    .then(data => {
      // Handle the response, e.g., show a success message or handle errors.
    	Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'User details updated sucessfully'
				})
    	setFirstName('')
    	setLastName('')
    	setMobileNo('')
    	})


    .catch(error => {
      // Handle errors, e.g., show an error message to the user.
    	Swal.fire({
					title: 'Something went Wrong!',
					icon: 'error',
					text: 'Try again'
				})
    });
  };

  return (
    <Form>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={e => setFirstName(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={e => setLastName(e.target.value)}
        />
      </Form.Group>

      <Form.Group controlId="mobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter mobile number"
          value={mobileNo}
          onChange={e => setMobileNo(e.target.value)}
        />
      </Form.Group>

      <Button variant="primary" onClick={handleUpdate}>
        Update Profile
      </Button>
    </Form>
  );
}
