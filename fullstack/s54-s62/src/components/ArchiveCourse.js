import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';




export default function ArchiveCourse({course, isActive, fetchData}){
	
	const archiveCourse = ( courseId ) => {


		fetch(`${process.env.REACT_APP_API_URL}/courses/${course}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Archived'
				})

				fetchData();

			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
			
			}
		})
	}

	const activateCourse = ( courseId ) => {


		fetch(`${process.env.REACT_APP_API_URL}/courses/${course}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Course Activated'
				})

				fetchData();
				
			} else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'please try again'
				})
			
			}
		})
	}


	return (

		(isActive)?
			<Button variant="danger" size="sm" onClick={() => archiveCourse(course)}> Archive </Button>
		:
			<Button variant="success" size="sm" onClick={() => activateCourse(course)}> Activate </Button>

	)
}