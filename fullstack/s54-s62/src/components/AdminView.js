import { useEffect, useState } from 'react';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';




export default function AdminView({ coursesData, fetchData }) {

	console.log(coursesData);

	const [courses, setCourses] = useState([])

	useEffect(() => {
		const courseHolder = coursesData.map(course => {
			return (
				<tr key={course._id}>
	            	<td>{course._id}</td>
	              	<td>{course.name}</td>
	              	<td>{course.description}</td>
	              	<td>{course.price}</td>
	              	<td>
		              	{
		              		course.isActive ? 
		              		(
		              			<p className="link-success"> Available </p>
		              		)
		              		:
		              		(
		              			<p className="link-danger"> Unavailable </p>
		              		)
		              	}
	              	</td>
		            <td>
		            	<EditCourse course={course._id} fetchData={fetchData} />
		            </td>
		            <td>
		            	<ArchiveCourse course={course._id} isActive={course.isActive} fetchData={fetchData} />
		            </td>
            	</tr>
			)
		})

		setCourses(courseHolder)
	}, [coursesData])

	return (
	    <div>
	      	<h2 className="text-center mt-4 mb-4">Admin Dashboard</h2>
	      		
      		<table className="table table-striped table-bordered table-hover table-responsive">
        		<thead className="text-center">
          			<tr>
			            <th>ID</th>
			            <th>Name</th>
			            <th>Description</th>
			            <th>Price</th>
			            <th>Availability</th>
			            <th colSpan="2">Actions</th>
		          	</tr>
        		</thead>
        		<tbody>
          
        		{ courses }

        		</tbody>
      		</table>
		</div>
  );
}


