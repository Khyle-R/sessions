import React, { useState, useEffect } from 'react';
import CourseCard from './CourseCard';
import CourseSearch from './CourseSearch.js'
import CourseSearchPrice from './CourseSearchPrice.js'




export default function UserView({ coursesData }) {
	const [activeCourses, setActiveCourses] = useState([]);

  	useEffect(() => {
    
    	const courseHolder = coursesData.map(course => {
    		if(course.isActive === true){
    			return (
    				<CourseCard key={course.id} course={course} />
    			)
    		} else {
    			return null;
    		}

    	});
    	setActiveCourses(courseHolder);
	
	}, [coursesData]);

  	return (
    	<>

      		<CourseSearch />
      		<CourseSearchPrice />
      		{ activeCourses }
      		
    	</>
  	);
}

