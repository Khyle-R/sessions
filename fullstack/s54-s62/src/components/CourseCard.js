import {Card} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function CourseCard({course}){

	// Destructuring the contents of 'course'
	const {name, description, price, _id} = course;

	// A state is just like a variable but with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state.
	// const [count, setCount] = useState(0);
	// const [seatCount, setSeatCount] = useState(30);

	// function enroll(){
	// 	// The setCount function can use the previous value of the state and add/modify to it.
	// 	setCount(prev_value => prev_value + 1);
	// 	setSeatCount(value => value - 1);

	// 	if (seatCount === 0) {
	// 		return alert("seats no longer available");
	// 	}
	// }

	return(
		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text> 

				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP{price}</Card.Text>

			    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
			</Card.Body>
		</Card>
	)
}

// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}