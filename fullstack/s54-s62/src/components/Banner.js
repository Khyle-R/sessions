import React from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

export default function Banner({ title, subtitle, buttonText, redirectPath}){
	const navigate = useNavigate();
	
	const Redirect = () => {	

		if(redirectPath == "notfound"){
			navigate("/");
		}

		if(redirectPath == "home"){
			navigate("/courses");
		}

 	};

	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>{ title }</h1>
				<p>{ subtitle }</p>
				<Button variant="primary" onClick={Redirect} >{ buttonText }</Button>
			</Col>
		</Row>
	)
}