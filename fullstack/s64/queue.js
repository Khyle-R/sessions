let collection = [];


function print() {
    return collection
}

function enqueue(element) {
    collection.push(element);

    return collection;
}

function dequeue() {
    collection.shift();
    
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

function newPerson(name) {
    // this.name = name;
    collection.push(name);

    return collection;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
