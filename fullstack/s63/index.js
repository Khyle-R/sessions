function countLetter(letter, sentence) {
    
    // Check first whether the letter is a single character.

    if (typeof letter !== 'string' || letter.length !== 1) {
        return undefined;
    }
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    let result = 0;

    for (let i = 0; i < sentence.length; i++) {
    
        if (sentence.charAt(i).toLowerCase() === letter.toLowerCase()) {
            result++;
        }
    }

    return result;
    // If letter is invalid, return undefined.

}


function isIsogram(text) {

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    const lowercaseText = text.toLowerCase();
    const letterCount = {};

    for (let i = 0; i < lowercaseText.length; i++) {
        const char = lowercaseText[i];
        
        if (letterCount[char]) {
        
            return false;
        
        } else {
        
            letterCount[char] = true;
        
        }
    }

    // If the function finds a repeating letter, return false. Otherwise, return true.
    return true;
    
}

function purchase(age, price) {
    
    if(age < 13){
        // Return undefined for people aged below 13.
        return undefined;   

    } else if (age > 12 && age < 22 || age > 64) {
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        const priceString = (price * 0.8).toFixed(2);
        return  priceString.toString();
        
    } else if (age > 21 && age < 65){
        // Return the rounded off price for people aged 22 to 64.
        const stringPrice = price.toFixed(2);
        return stringPrice.toString();
    
    } else {

        return "Invalid input";
    } 
    
    
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    const noStocks = {};
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    for (const item of items) {
        if (item.stocks === 0) {
            noStocks[item.category] = true;
        }
    }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    return Object.keys(noStocks);

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

   
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    const sameVoters = [];

    for (const voterA of candidateA){
        if (candidateB.includes(voterA)) {
            sameVoters.push(voterA);
        }
    }

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    return sameVoters;
    
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};